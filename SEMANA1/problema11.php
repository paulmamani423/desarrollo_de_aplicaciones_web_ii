<!DOCTYPE html>
<html>
<head>
	<title>Determinar si una persona es mayor o menor de edad</title>
</head>
<body>
	<form method="post">
		<label>Ingrese la edad:</label>
		<input type="number" name="edad">
		<br><br>
		<input type="submit" name="verificar" value="Verificar">
	</form>
	<br>
	<?php
		if(isset($_POST["verificar"])){
			$edad = $_POST["edad"];
			if($edad >= 18){
				echo "La persona es mayor de edad.";
			} else {
				echo "La persona es menor de edad.";
			}
		}
	?>
</body>
</html>
