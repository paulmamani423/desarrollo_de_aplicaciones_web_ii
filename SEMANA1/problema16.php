<!DOCTYPE html>
<html>
<head>
	<title>Promedio y aprobación</title>
</head>
<body>
	<h1>Promedio y aprobación</h1>
	<form method="post" action="">
		<label for="nota1">Nota 1:</label>
		<input type="number" name="nota1" id="nota1" required><br><br>
		<label for="nota2">Nota 2:</label>
		<input type="number" name="nota2" id="nota2" required><br><br>
		<label for="nota3">Nota 3:</label>
		<input type="number" name="nota3" id="nota3" required><br><br>
		<label for="nota4">Nota 4:</label>
		<input type="number" name="nota4" id="nota4" required><br><br>
		<input type="submit" value="Calcular">
	</form>
	<br>
	<?php
		if ($_SERVER['REQUEST_METHOD'] == 'POST') {
			$notas = array($_POST['nota1'], $_POST['nota2'], $_POST['nota3'], $_POST['nota4']);
			rsort($notas);
			$promedio = ($notas[0] + $notas[1] + $notas[2]) / 3;
			if ($promedio >= 11) {
				echo "Aprobado con un promedio de $promedio";
			} else {
				echo "Desaprobado con un promedio de $promedio";
			}
		}
	?>
</body>
</html>
