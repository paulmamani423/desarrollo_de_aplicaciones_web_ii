<!DOCTYPE html>
<html>
<head>
	<title>Área y Perímetro de un Cuadrado</title>
</head>
<body>
	<form method="post">
		<label>Ingrese el lado del cuadrado:</label>
		<input type="number" name="lado">
		<br><br>
		<input type="submit" name="calcular" value="Calcular">
	</form>
	<br>
	<?php
		if(isset($_POST["calcular"])){
			$lado = $_POST["lado"];
			$area = pow($lado, 2);
			$perimetro = $lado * 4;
			echo "El área del cuadrado es: ".$area."<br>";
			echo "El perímetro del cuadrado es: ".$perimetro;
		}
	?>
</body>
</html>
