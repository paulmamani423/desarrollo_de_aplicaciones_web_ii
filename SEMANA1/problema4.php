<!DOCTYPE html>
<html>
<head>
	<title>Obtener los valores de "c" y "d"</title>
</head>
<body>
	<h1>Obtener los valores de "c" y "d"</h1>
	<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
		<label for="a">Ingrese el valor de "a":</label>
		<input type="number" id="a" name="a" required><br>

		<label for="b">Ingrese el valor de "b":</label>
		<input type="number" id="b" name="b" required><br>

		<input type="submit" value="Calcular">
	</form>

	<?php
		if(isset($_POST["a"]) && isset($_POST["b"])) {
			$a = $_POST["a"];
			$b = $_POST["b"];

			// Calcular los valores de "c" y "d"
			$c = sqrt(pow($a, 2) + pow($b, 2));
			$d = sqrt(pow($c, 2) - pow($a, 2));

			// Mostrar resultados
			echo "<p>Para a = $a y b = $b, los valores de c y d son:</p>";
			echo "<ul>";
			echo "<li>c = $c</li>";
			echo "<li>d = $d</li>";
			echo "</ul>";
		}
	?>
</body>
</html>
