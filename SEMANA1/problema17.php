<?php
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  $saldo_anterior = $_POST["saldo_anterior"];
  $tipo_movimiento = $_POST["tipo_movimiento"];
  $monto_transaccion = $_POST["monto_transaccion"];

  // Actualizar el saldo dependiendo del tipo de movimiento
  if ($tipo_movimiento == "R") {
    $saldo_actual = $saldo_anterior - $monto_transaccion;
  } else {
    $saldo_actual = $saldo_anterior + $monto_transaccion;
  }
} else {
  // Valores iniciales para evitar errores
  $saldo_anterior = 0;
  $tipo_movimiento = "D";
  $monto_transaccion = 0;
  $saldo_actual = $saldo_anterior + $monto_transaccion;
}
?>

<!DOCTYPE html>
<html>
<head>
  <title>Actualizar saldo</title>
</head>
<body>
  <h1>Actualizar saldo</h1>
  <form method="post">
    <label>Saldo anterior:</label>
    <input type="number" name="saldo_anterior" value="<?php echo $saldo_anterior; ?>"><br>

    <label>Tipo de movimiento:</label>
    <select name="tipo_movimiento">
      <option value="D" <?php if ($tipo_movimiento == "D") { echo "selected"; } ?>>Depósito</option>
      <option value="R" <?php if ($tipo_movimiento == "R") { echo "selected"; } ?>>Retiro</option>
    </select><br>

    <label>Monto de la transacción:</label>
    <input type="number" name="monto_transaccion" value="<?php echo $monto_transaccion; ?>"><br>

    <label>Saldo actual:</label>
    <input type="number" name="saldo_actual" value="<?php echo $saldo_actual; ?>" readonly><br>

    <input type="submit" value="Actualizar">
  </form>
</body>
</html>
