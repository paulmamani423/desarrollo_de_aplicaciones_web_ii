<!DOCTYPE html>
<html>
<head>
	<title>Determinar cuál número es mayor</title>
</head>
<body>
	<form method="post" action="">
		<label for="num1">Ingrese el primer número:</label>
		<input type="number" id="num1" name="num1"><br>

		<label for="num2">Ingrese el segundo número:</label>
		<input type="number" id="num2" name="num2"><br>

		<input type="submit" value="Determinar">
	</form>
	
	<?php
		if (isset($_POST['num1']) && isset($_POST['num2'])) {
			$num1 = $_POST['num1'];
			$num2 = $_POST['num2'];

			if ($num1 > $num2) {
				echo "<p>El número $num1 es mayor que el número $num2.</p>";
			} elseif ($num2 > $num1) {
				echo "<p>El número $num2 es mayor que el número $num1.</p>";
			} else {
				echo "<p>Los números $num1 y $num2 son iguales.</p>";
			}
		}
	?>
</body>
</html>
