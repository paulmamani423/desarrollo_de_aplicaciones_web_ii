<!DOCTYPE html>
<html>
<head>
	<title>Comparación de números</title>
</head>
<body>
	<h1>Comparación de números</h1>
	<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
		<label for="num1">Primer número:</label>
		<input type="number" id="num1" name="num1" required><br><br>
		<label for="num2">Segundo número:</label>
		<input type="number" id="num2" name="num2" required><br><br>
		<input type="submit" value="Comparar">
	</form>
	<?php
		if ($_SERVER["REQUEST_METHOD"] == "POST") {
			$num1 = $_POST["num1"];
			$num2 = $_POST["num2"];

			if ($num1 == $num2) {
				echo "<p>Los números son iguales.</p>";
			} else {
				echo "<p>Los números son diferentes.</p>";
			}
		}
	?>
</body>
</html>
