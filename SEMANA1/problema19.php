<!DOCTYPE html>
<html>
<head>
	<title>Verificar si forman un triángulo</title>
</head>
<body>
	<form method="post">
		<label for="lado1">Ingrese la longitud del primer lado:</label>
		<input type="number" name="lado1"><br>

		<label for="lado2">Ingrese la longitud del segundo lado:</label>
		<input type="number" name="lado2"><br>

		<label for="lado3">Ingrese la longitud del tercer lado:</label>
		<input type="number" name="lado3"><br>

		<input type="submit" name="submit" value="Verificar">
	</form>

	<?php
	if(isset($_POST['submit'])){
		$lado1 = $_POST['lado1'];
		$lado2 = $_POST['lado2'];
		$lado3 = $_POST['lado3'];

		if (($lado1 < ($lado2 + $lado3)) && ($lado2 < ($lado1 + $lado3)) && ($lado3 < ($lado1 + $lado2))) {
			echo "Estas longitudes SI forman un triángulo.";
		}
		else {
			echo "Estas longitudes NO forman un triángulo.";
		}
	}
	?>
</body>
</html>
