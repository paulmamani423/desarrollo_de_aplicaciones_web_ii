<!DOCTYPE html>
<html>
<head>
	<title>Calcular Área y Perímetro de un Rectángulo</title>
</head>
<body>
	<form method="post">
		<label>Ingrese la base del rectángulo:</label>
		<input type="number" name="base">
		<br><br>
		<label>Ingrese la altura del rectángulo:</label>
		<input type="number" name="altura">
		<br><br>
		<input type="submit" name="calcular" value="Calcular">
	</form>
	<br>
	<?php
		if(isset($_POST["calcular"])){
			$base = $_POST["base"];
			$altura = $_POST["altura"];
			$area = $base * $altura;
			$perimetro = 2 * ($base + $altura);
			echo "El área del rectángulo es: ".$area."<br>";
			echo "El perímetro del rectángulo es: ".$perimetro;
		}
	?>
</body>
</html>
