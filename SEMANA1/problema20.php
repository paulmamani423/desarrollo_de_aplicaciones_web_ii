<!DOCTYPE html>
<html>
<head>
	<title>Tipo de triángulo</title>
</head>
<body>
	<form method="post">
		<label for="lado1">Lado 1:</label>
		<input type="number" name="lado1" required><br><br>
		<label for="lado2">Lado 2:</label>
		<input type="number" name="lado2" required><br><br>
		<label for="lado3">Lado 3:</label>
		<input type="number" name="lado3" required><br><br>
		<button type="submit" name="submit">Determinar tipo de triángulo</button>
	</form>
	<?php
		if (isset($_POST['submit'])) {
			$lado1 = $_POST['lado1'];
			$lado2 = $_POST['lado2'];
			$lado3 = $_POST['lado3'];
			
			if (($lado1 + $lado2 > $lado3) && ($lado1 + $lado3 > $lado2) && ($lado2 + $lado3 > $lado1)) {
				if ($lado1 == $lado2 && $lado2 == $lado3) {
					echo "El triángulo es equilátero.";
				} elseif ($lado1 == $lado2 || $lado1 == $lado3 || $lado2 == $lado3) {
					echo "El triángulo es isósceles.";
				} else {
					echo "El triángulo es escaleno.";
				}
			} else {
				echo "Los lados ingresados no forman un triángulo.";
			}
		}
	?>
</body>
</html>
