<!DOCTYPE html>
<html>
<head>
	<title>Convertir Horas a Minutos y Segundos</title>
</head>
<body>
	<form method="post">
		<label>Ingrese la cantidad de horas:</label>
		<input type="number" name="horas">
		<br><br>
		<input type="submit" name="calcular" value="Calcular">
	</form>
	<br>
	<?php
		if(isset($_POST["calcular"])){
			$horas = $_POST["horas"];
			$minutos = $horas * 60;
			$segundos = $horas * 3600;
			echo $horas." horas equivalen a ".$minutos." minutos y ".$segundos." segundos.";
		}
	?>
</body>
</html>
