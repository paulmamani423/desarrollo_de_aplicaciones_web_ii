<!DOCTYPE html>
<html>
<head>
	<title>Contar números enteros incluidos</title>
</head>
<body>
	<h1>Contar números enteros incluidos</h1>
	<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
		<label for="a">Ingrese el primer número entero:</label>
		<input type="number" id="a" name="a" required><br>

		<label for="b">Ingrese el segundo número entero:</label>
		<input type="number" id="b" name="b" required><br>

		<input type="submit" value="Contar">
	</form>

	<?php
		if(isset($_POST["a"]) && isset($_POST["b"])) {
			$a = $_POST["a"];
			$b = $_POST["b"];

			$count = abs($b - $a) + 1;

			echo "<p>Hay $count números enteros incluidos entre $a y $b.</p>";
		}
	?>
</body>
</html>
