<!DOCTYPE html>
<html>
<head>
	<title>Convertir Grados Fahrenheit a Celsius y Kelvin</title>
</head>
<body>
	<form method="post">
		<label>Ingrese la cantidad de grados Fahrenheit:</label>
		<input type="number" name="fahrenheit">
		<br><br>
		<input type="submit" name="calcular" value="Calcular">
	</form>
	<br>
	<?php
		if(isset($_POST["calcular"])){
			$fahrenheit = $_POST["fahrenheit"];
			$celsius = ($fahrenheit - 32) * (5/9);
			$kelvin = $celsius + 273.15;
			echo $fahrenheit." grados Fahrenheit equivalen a ".$celsius." grados Celsius y ".$kelvin." grados Kelvin.";
		}
	?>
</body>
</html>
