<!DOCTYPE html>
<html>
<head>
	<title>Conversión de milímetros a metros, decímetros, centímetros y milímetros</title>
</head>
<body>
	<h1>Conversión de milímetros a metros, decímetros, centímetros y milímetros</h1>
	<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
		<label for="milimetros">Ingrese la cantidad de milímetros:</label>
		<input type="number" id="milimetros" name="milimetros" required><br>

		<input type="submit" value="Convertir">
	</form>

	<?php
		if(isset($_POST["milimetros"])) {
			$milimetros = $_POST["milimetros"];

			// Conversión de milímetros a metros, decímetros, centímetros y milímetros
			$metros = floor($milimetros / 1000);
			$decimetros = floor(($milimetros % 1000) / 100);
			$centimetros = floor(($milimetros % 100) / 10);
			$milimetros_restantes = $milimetros % 10;

			// Mostrar resultados
			echo "<p>$milimetros milímetros son equivalentes a $metros metros, $decimetros decímetros, $centimetros centímetros y $milimetros_restantes milímetros.</p>";
		}
	?>
</body>
</html>
