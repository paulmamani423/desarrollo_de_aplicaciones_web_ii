<!DOCTYPE html>
<html>
<head>
	<title>Doble, triple o cero</title>
</head>
<body>
	<h1>Doble, triple o cero</h1>
	<form method="post" action="">
		<label for="numero">Ingrese un número:</label>
		<input type="number" name="numero" id="numero" required>
		<input type="submit" value="Calcular">
	</form>
	<?php
		if(isset($_POST['numero'])){
			$numero = $_POST['numero'];
			if($numero > 0){
				echo "<p>El doble del número ingresado es: " . ($numero * 2) . "</p>";
			} else if($numero < 0){
				echo "<p>El triple del número ingresado es: " . ($numero * 3) . "</p>";
			} else {
				echo "<p>El número ingresado es cero.</p>";
			}
		}
	?>
</body>
</html>
