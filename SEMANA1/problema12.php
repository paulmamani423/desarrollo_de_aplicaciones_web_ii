<!DOCTYPE html>
<html>
<head>
	<title>Devolver el número menor</title>
</head>
<body>
	<form method="post">
		<label>Ingrese el primer número:</label>
		<input type="number" name="num1">
		<br><br>
		<label>Ingrese el segundo número:</label>
		<input type="number" name="num2">
		<br><br>
		<input type="submit" name="buscar" value="Buscar">
	</form>
	<br>
	<?php
		if(isset($_POST["buscar"])){
			$num1 = $_POST["num1"];
			$num2 = $_POST["num2"];
			if($num1 < $num2){
				echo "El número menor es: ".$num1;
			} else {
				echo "El número menor es: ".$num2;
			}
		}
	?>
</body>
</html>
