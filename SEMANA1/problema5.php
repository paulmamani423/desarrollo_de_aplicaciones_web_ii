<!DOCTYPE html>
<html>
<head>
	<title>Calcular el porcentaje de cada número</title>
</head>
<body>
	<h1>Calcular el porcentaje de cada número</h1>
	<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
		<label for="num1">Ingrese el primer número:</label>
		<input type="number" id="num1" name="num1" required><br>

		<label for="num2">Ingrese el segundo número:</label>
		<input type="number" id="num2" name="num2" required><br>

		<label for="num3">Ingrese el tercer número:</label>
		<input type="number" id="num3" name="num3" required><br>

		<label for="num4">Ingrese el cuarto número:</label>
		<input type="number" id="num4" name="num4" required><br>

		<input type="submit" value="Calcular">
	</form>

	<?php
		if(isset($_POST["num1"]) && isset($_POST["num2"]) && isset($_POST["num3"]) && isset($_POST["num4"])) {
			$num1 = $_POST["num1"];
			$num2 = $_POST["num2"];
			$num3 = $_POST["num3"];
			$num4 = $_POST["num4"];

			// Calcular la suma de los 4 números
			$suma = $num1 + $num2 + $num3 + $num4;

			// Calcular el porcentaje de cada número
			$porcentaje1 = ($num1 / $suma) * 100;
			$porcentaje2 = ($num2 / $suma) * 100;
			$porcentaje3 = ($num3 / $suma) * 100;
			$porcentaje4 = ($num4 / $suma) * 100;

			// Mostrar resultados
			echo "<p>Para los números $num1, $num2, $num3 y $num4, los porcentajes son:</p>";
			echo "<ul>";
			echo "<li>$num1: $porcentaje1%</li>";
			echo "<li>$num2: $porcentaje2%</li>";
			echo "<li>$num3: $porcentaje3%</li>";
			echo "<li>$num4: $porcentaje4%</li>";
			echo "</ul>";
		}
	?>
</body>
</html>
