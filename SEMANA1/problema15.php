<!DOCTYPE html>
<html>
  <head>
    <title>Ordenar números</title>
  </head>
  <body>
    <form method="post">
      <label>Ingrese el primer número:</label>
      <input type="number" name="num1"><br><br>
      <label>Ingrese el segundo número:</label>
      <input type="number" name="num2"><br><br>
      <label>Ingrese el tercer número:</label>
      <input type="number" name="num3"><br><br>
      <input type="submit" value="Ordenar">
    </form>
    <?php
      if (isset($_POST["num1"]) && isset($_POST["num2"]) && isset($_POST["num3"])) {
        $num1 = $_POST["num1"];
        $num2 = $_POST["num2"];
        $num3 = $_POST["num3"];
        // Ordenar en forma ascendente
        $ascendente = array($num1, $num2, $num3);
        sort($ascendente);
        echo "Números ordenados en forma ascendente: ";
        foreach ($ascendente as $num) {
          echo $num . " ";
        }
        echo "<br>";
        // Ordenar en forma descendente
        $descendente = array($num1, $num2, $num3);
        rsort($descendente);
        echo "Números ordenados en forma descendente: ";
        foreach ($descendente as $num) {
          echo $num . " ";
        }
      }
    ?>
  </body>
</html>
