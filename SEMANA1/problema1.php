<!DOCTYPE html>
<html>
<head>
	<title>Suma y Resta de números enteros</title>
</head>
<body>
	<h1>Suma y Resta de números enteros</h1>
	<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]); ?>" method="post">
		<label for="a">Ingrese el valor de a:</label>
		<input type="number" id="a" name="a" required><br>

		<label for="b">Ingrese el valor de b:</label>
		<input type="number" id="b" name="b" required><br>

		<input type="submit" value="Calcular">
	</form>

	<?php
		if(isset($_POST["a"]) && isset($_POST["b"])) {
			$a = $_POST["a"];
			$b = $_POST["b"];

			$suma = $a + $b;
			$resta = $a - $b;

			echo "<p>El resultado de la suma de $a y $b es: $suma</p>";
			echo "<p>El resultado de la resta de $a y $b es: $resta</p>";
		}
	?>
</body>
</html>
