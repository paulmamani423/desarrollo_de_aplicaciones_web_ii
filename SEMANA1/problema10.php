<!DOCTYPE html>
<html>
<head>
	<title>Conversión de grados sexagesimales a centesimales</title>
</head>
<body>
	<form method="post">
		<label>Ingrese el valor en grados sexagesimales:</label>
		<input type="number" name="grados">
		<br><br>
		<input type="submit" name="convertir" value="Convertir">
	</form>
	<br>
	<?php
		if(isset($_POST["convertir"])){
			$grados = $_POST["grados"];
			$centesimales = ($grados * 100) / 90;
			echo $grados." grados sexagesimales son ".$centesimales." grados centesimales.";
		}
	?>
</body>
</html>
