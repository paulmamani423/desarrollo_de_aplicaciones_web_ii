<!DOCTYPE html>
<html>
<head>
	<title>Calculo de sumas y cantidades</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            margin: 20px;
        }

        label {
            display: block;
            margin-bottom: 10px;
        }

        input[type="number"] {
            padding: 5px;
            border-radius: 5px;
            border: 1px solid #ccc;
            margin-bottom: 10px;
        }

        input[type="submit"] {
            background-color: #966FD6;
            color: #fff;
            border: none;
            padding: 10px 20px;
            border-radius: 5px;
            cursor: pointer;
        }

        h2 {
            font-size: 24px;
            margin-bottom: 10px;
        }

        p {
            margin-bottom: 5px;
        }

        .resultados {
            border: 1px solid #ccc;
            padding: 10px;
            border-radius: 5px;
            margin-top: 20px;
        }
    </style>
</head>
<body>
	<form method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
		Ingrese valor de a: <input type="number" name="a"><br>
		Ingrese valor de b: <input type="number" name="b"><br>
		<input type="submit" value="Calcular">
	</form>

	<?php
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
	    $a = $_POST["a"];
	    $b = $_POST["b"];

	    $suma_pares = 0; 
	    $cantidad_pares = 0; 
	    $suma_impares = 0; 
	    $cantidad_impares = 0; 
	    $suma_multiplos_3 = 0; 
	    $cantidad_multiplos_3 = 0; 

	    for ($i = $a; $i <= $b; $i++) {
	        if ($i % 2 == 0) { 
	            $suma_pares += $i; 
	            $cantidad_pares++; 
	        } else { 
	            $suma_impares += $i; 
	            $cantidad_impares++; 
	        }
	        if ($i % 3 == 0) { 
	            $suma_multiplos_3 += $i; 
	            $cantidad_multiplos_3++; 
	        }
	    }

	    echo "La suma de numeros pares entre ($a, $b) es: $suma_pares <br>";
	    echo "La cantidad de numeros pares entre ($a, $b) es: $cantidad_pares <br>";
	    echo "La suma de numeros impares entre ($a, $b) es: $suma_impares <br>";
	    echo "La cantidad de numeros impares entre ($a, $b) es: $cantidad_impares <br>";
	    echo "La suma de multiplos de 3 entre ($a, $b) es: $suma_multiplos_3 <br>";
	    echo "La cantidad de multiplos de 3 entre ($a, $b) es: $cantidad_multiplos_3 <br>";
	}
	?>
</body>
</html>
