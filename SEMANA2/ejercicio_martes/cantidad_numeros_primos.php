<!DOCTYPE html>
<html>
<head>
	<title>Cantidad de números primos de n cifras</title>
	<style>
		h1 {
			text-align: center;
			text-transform: uppercase;
			margin-bottom: 30px;
		}
		label {
			font-weight: bold;
			margin-right: 10px;
		}
		input[type="number"] {
			padding: 5px;
			margin-right: 10px;
		}
		input[type="submit"] {
			background-color: #4CAF50;
			border: none;
			color: white;
			padding: 10px 20px;
			text-align: center;
			text-decoration: none;
			display: inline-block;
			font-size: 16px;
			margin-top: 10px;
			cursor: pointer;
		}
	</style>
</head>
<body>
	<h1>Cantidad de números primos de n cifras</h1>

	<form method="POST" action="">
		<label for="n">Ingrese el número de cifras:</label>
		<input type="number" id="n" name="n" min="1" required><br><br>
		<input type="submit" name="submit" value="Calcular">
	</form>

	<?php
	if(isset($_POST['submit'])) {
	    $n = $_POST["n"];
	    $count = 0;

	    for ($i = pow(10, $n-1); $i < pow(10, $n); $i++) {
	        $is_prime = true;
	        for ($j = 2; $j <= sqrt($i); $j++) {
	            if ($i % $j == 0) {
	                $is_prime = false;
	                break;
	            }
	        }
	        if ($is_prime && $i != 1) {
	            $count++;
	        }
	    }
	    echo "<p>La cantidad de números primos de $n cifras es: $count</p>";
	}
	?>
</body>
</html>

