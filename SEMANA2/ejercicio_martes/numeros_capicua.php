<!DOCTYPE html>
<html>
<head>
	<title>CONTAR NÚMERO CAPICÚA</title>
    <style>
        h1{
            text-transform: uppercase;
        }
        body {
            font-family: Arial, sans-serif;
            margin: 20px;
        }
        label {
            display: block;
            margin-bottom: 10px;
        }
        input[type="number"] {
            padding: 5px;
            border-radius: 5px;
            border: 1px solid #ccc;
            margin-bottom: 10px;
        }
        input[type="submit"] {
            background-color: 	#966FD6 ;
            color: #fff;
            border: none;
            padding: 10px 20px;
            border-radius: 5px;
            cursor: pointer;
        }
        h2 {
            font-size: 24px;
            margin-bottom: 10px;
        }
        p {
            margin-bottom: 5px;
        }
        .resultados {
            border: 1px solid #ccc;
            padding: 10px;
            border-radius: 5px;
            margin-top: 20px;
        }
    </style>
</head>
<body>
	<h1>Contar números capicúa</h1>
	<form method="post" action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]);?>">
		<label for="inicio">Inicio:</label>
		<input type="number" name="inicio" id="inicio"><br>
		<label for="fin">Fin:</label>
		<input type="number" name="fin" id="fin"><br>
		<input type="submit" value="Contar">
	</form>
	<?php
	if ($_SERVER["REQUEST_METHOD"] == "POST") {
		$inicio = $_POST["inicio"];
		$fin = $_POST["fin"];
		$count = 0;
		for ($i=$inicio; $i<=$fin; $i++) {
			if (strval($i) == strrev(strval($i))) {
				$count++;
			}
		}
		echo "<p>Hay $count numeros capicua en el rango $inicio-$fin</p>";
	}
	?>
</body>
</html>
