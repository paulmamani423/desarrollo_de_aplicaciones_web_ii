<?php
if(isset($_POST['submit'])) {
    $x = $_POST["x"];
    $n = $_POST["n"];

    $s = 1; 
    $factorial = 1; 

    for ($i = 1; $i <= $n; $i++) {
        $factorial *= $i; 
        $s += pow($x, $i) / $factorial; 
    }
}
?>

<form method="POST" action="">
    <label for="x">Ingrese el valor de x:</label>
    <input type="number" id="x" name="x"><br><br>
    <label for="n">Ingrese el valor de n:</label>
    <input type="number" id="n" name="n"><br><br>
    <input type="submit" name="submit" value="Calcular">
</form>

<?php if(isset($_POST['submit'])) { ?>
    <h2>Resultado:</h2>
    <p>La suma de la serie es: <?php echo $s; ?></p>
<?php } ?>
