<!--Lea una frase y una palabra, y determine si existe o no la palabra en la frase.-->
<?php
//Variables
$f1 = ""; $f2 = ""; $c = "";
$i = 0; $p = 0;
if(isset($_POST["btnCalcular"])) {
//Entrada
    $f1 = $_POST["txtf1"];
//Proceso
    $p = 0;
    $f1 = trim($f1);
    for($i = 0; $i<strlen($f1); $i++){
        $c = substr($f1,$i,1);
        if($c != "") {
        $c = chr(ord($c) + 1);
        }
        $f2 .= $c;
        }
        }
?>
<html>
    <head>
    <title>Problema 65</title>
    <style type=”text/css”>
        .TextoFondo {
        background-color: greenyellow;
        }
        .Frase {
            background-color: grey;
        }
        .encriptada {
            background-color: grey;
        }
        </style>
            </head>
                <body>
                    <form method="post" action="Propuesto_65.php">
                        <table width="344" border="0">
                            <tr>
                                <td colspan="2"><strong>Problema 65</strong> </td>
                            </tr>
                            <tr>
                                <td width="86">Frase</td>
                                <td width="172"><textarea class = "Frase" name="txtf1" cols="40" rows="5" id="txtf1"><?=$f1?></textarea></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td>Frase encriptada </td>
                                <td>
                                    <textarea class="encriptada" name="txtf2" cols="40" rows="5" class="TextoFondo" id="txtf2"><?=$f2?> </textarea>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <input name="btnCalcular" type="submit" id="btnCalcular" value="Calcular" /> </td>
                            </tr>
                        </table>
                    </form>
                </body>
</html>