<!DOCTYPE html>
<html>
<head>
	<title>Verificar si una palabra es palindromo</title>
	<style>
		h1 {
			font-family: Arial, sans-serif;
			font-size: 24px;
            text-transform: uppercase;
		}
		input[type="text"] {
			font-family: Arial, sans-serif;
			font-size: 16px;
			padding: 6px;
		}
		button {
			font-family: Arial, sans-serif;
			font-size: 16px;
			padding: 6px 12px;
			background-color: #007bff;
			color: #fff;
			border: none;
			border-radius: 4px;
			cursor: pointer;
		}
	</style>
</head>
<body>
	<div>
		<h1>Verificar si una palabra es palindromo</h1>
		<form method="post">
			<label for="palabra">Palabra:</label>
			<input type="text" id="palabra" name="palabra" required>
			<button type="submit">Verificar</button>
		</form>
		<?php
			function es_palindromo($palabra) {
				$palabra_reversa = strrev($palabra);
				return $palabra == $palabra_reversa;
			}

			if ($_SERVER["REQUEST_METHOD"] == "POST") {
				$palabra = $_POST["palabra"];
				if (es_palindromo($palabra)) {
					echo "<p>La palabra '$palabra' es un palindromo.</p>";
				} else {
					echo "<p>La palabra '$palabra' no es un palindromo.</p>";
				}
			}
		?>
	</div>
</body>
</html>
