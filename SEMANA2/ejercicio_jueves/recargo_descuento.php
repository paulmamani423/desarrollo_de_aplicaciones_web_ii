<!DOCTYPE html>
<html>
<head>
	<title>DESCUENTO RECARGO</title>
	<style>
        h1 {
            text-transform: uppercase;
        }
		form {
			display: flex;
			flex-direction: column;
			align-items: center;
			margin-top: 50px;

		}
		input[type="submit"] {
			margin-top: 20px;
			padding: 10px 20px;
			font-size: 16px;
			background-color: #007bff;
			color: #fff;
			border: none;
			border-radius: 5px;
			cursor: pointer;
		}
	</style>
</head>
<body>
	<form method="post">
		<label for="tipo">Tipo de cliente:</label>
		<select name="tipo" id="tipo">
			<option value="G">Publico en general (G)</option>
			<option value="A">Cliente afiliado (A)</option>
		</select>
		<label for="forma_pago">Forma de pago:</label>
		<select name="forma_pago" id="forma_pago">
			<option value="C">Al contado (C)</option>
			<option value="P">En plazos (P)</option>
		</select>
		<label for="monto">Monto de la compra:</label>
		<input type="number" name="monto" id="monto" required>
		<input type="submit" value="Calcular">
	</form>
	<?php
		function recargo($tipo) {
			if ($tipo == "G") {
				return 10;
			} else {
				return 5;
			}
		}
		function descuento($tipo) {
			if ($tipo == "G") {
				return 15;
			} else {
				return 20;
			}
		}
		if (isset($_POST["tipo"]) && isset($_POST["forma_pago"]) && isset($_POST["monto"])) {
			$tipo = $_POST["tipo"];
			$forma_pago = $_POST["forma_pago"];
			$monto = $_POST["monto"];
			$descuento = 0;
			$recargo = 0;
			if ($forma_pago == "C") {
				$descuento = $monto * descuento($tipo) / 100;
			} else {
				$recargo = $monto * recargo($tipo) / 100;
			}
			$total = $monto - $descuento + $recargo;
			echo "<p>Total de la compra: $monto</p>";
			echo "<p>Descuento: $descuento</p>";
			echo "<p>Recargo: $recargo</p>";
			echo "<p>Total a pagar: $total</p>";
		}
	?>
</body>
</html>
