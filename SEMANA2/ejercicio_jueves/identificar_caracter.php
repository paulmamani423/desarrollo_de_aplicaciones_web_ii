<!DOCTYPE html>
<html>
<head>
	<title>CARACTER</title>
	<style>
		body {
			font-family: Arial, sans-serif;
			background-color: #f2f2f2;
		}
		h1 {
			text-align: center;
			color: #333;
            text-transform: uppercase;
		}
		form {
			width: 80%;
			margin: 0 auto;
			padding: 20px;
			background-color: #fff;
			border-radius: 5px;
			box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.3);
		}
		input[type="text"] {
			width: 100%;
			padding: 10px;
			margin: 10px 0;
			border-radius: 3px;
			border: 1px solid #ccc;
		}
		input[type="submit"] {
			display: block;
			width: 100%;
			padding: 10px;
			border-radius: 3px;
			border: none;
			background-color: #4CAF50;
			color: #fff;
			font-size: 16px;
			cursor: pointer;
		}
		.result {
			width: 80%;
			margin: 20px auto;
			padding: 20px;
			background-color: #fff;
			border-radius: 5px;
			box-shadow: 0px 0px 5px 0px rgba(0,0,0,0.3);
		}
		.result h2 {
			margin-top: 0;
		}
		.result p {
			margin: 10px 0;
			font-size: 18px;
			color: #333;
		}
	</style>
</head>
<body>
	<h1>Carácter</h1>
	<form method="POST">
		<label for="caracter">Introduce un carácter:</label>
		<input type="text" name="caracter" id="caracter" required>
		<input type="submit" value="Comprobar">
	</form>

	<?php
	if(isset($_POST["caracter"])) {
		$caracter = $_POST["caracter"];
		$resultado = "";

		if(ctype_alpha($caracter)) { // Si es letra
			if(ctype_upper($caracter)) { // Si es letra mayúscula
				$resultado = "Es una letra mayuscula";
			} else { // Si es letra minúscula
				$resultado = "Es una letra minuscula";
			}
			if(in_array(strtolower($caracter), array("a", "e", "i", "o", "u"))) { // Si es vocal
				$resultado .= " y es una vocal.";
			} else { // Si no es vocal
				$resultado .= " y no es una vocal.";
			}
		} elseif(ctype_digit($caracter)) { // Si es número
			$resultado = "Es un numero";
		} else { // Si es símbolo
			$resultado = "Es un simbolo";
		}

		echo '<div class="result"><h2>Resultado:</h2><p>' . $resultado . '</p></div>';
	}
	?>
</body>
</html>
