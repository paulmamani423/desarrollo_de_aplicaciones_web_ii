<?php
function Etapa($edad) {
  if ($edad >= 0 && $edad <= 2) {
    return "Bebe";
  } elseif ($edad >= 3 && $edad <= 5) {
    return "Niño";
  } elseif ($edad >= 6 && $edad <= 12) {
    return "Pubertad";
  } elseif ($edad >= 13 && $edad <= 18) {
    return "Adolescente";
  } elseif ($edad >= 19 && $edad <= 25) {
    return "Joven";
  } elseif ($edad >= 26 && $edad <= 60) {
    return "Adulto";
  } elseif ($edad >= 60) {
    return "Anciano";
  }
}

if(isset($_POST['enviar'])){
  $edad = $_POST['edad'];
  $etapa = Etapa($edad);
}
?>

<!DOCTYPE html>
<html>
<head>
  <title>DETERMINAR ETAPA DE LA VIDA</title>
  <style>
    body {
      background-color: #FFFFFF;
      font-family: Arial, sans-serif;
    }

    h1 {
      text-align: center;
      color: #D4AF37;
      text-transform: uppercase;
    }

    form {
      margin: 50px auto;
      max-width: 400px;
      border: 1px solid #1E90FF;
      border-radius: 5px;
      padding: 20px;
      background-color: #FFF;
      box-shadow: 0px 0px 10px #888888;
    }

    label {
      display: block;
      margin-bottom: 10px;
      color: #333;
    }

    input[type="number"] {
      display: block;
      width: 100%;
      padding: 10px;
      border: 1px solid #CCC;
      border-radius: 5px;
      box-sizing: border-box;
      margin-bottom: 20px;
    }

    input[type="submit"] {
      background-color: #D4AF37;
      color: #FFF;
      padding: 10px;
      border: none;
      border-radius: 5px;
      cursor: pointer;
      width: 100%;
      transition: background-color 0.3s ease-in-out;
    }

    input[type="submit"]:hover {
      background-color: #000000;
    }

    p {
      font-size: 20px;
      text-align: center;
      margin-top: 30px;
      color: #333;
    }
  </style>
</head>
<body>
  <h1>Determinar etapa de la vida</h1>
  <form method="post">
    <label>Ingrese la edad:</label>
    <input type="number" name="edad">
    <input type="submit" name="enviar" value="Enviar">
  </form>

  <?php if(isset($etapa)): ?>
    <p>La persona se encuentra en la etapa de: <?php echo $etapa ?></p>
  <?php endif; ?>
</body>
</html>

