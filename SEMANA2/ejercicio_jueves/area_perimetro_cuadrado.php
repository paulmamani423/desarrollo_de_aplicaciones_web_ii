<!DOCTYPE html>
<html>
<head>
	<title>area y Perimetro de un Cuadrado</title>
	<style>
		body {
			font-family: Arial, sans-serif;
			background-color: #f2f2f2;
		}
		h1 {
			text-align: center;
			margin-top: 30px;
            text-transform: uppercase;
		}
		form {
			margin-top: 30px;
			text-align: center;
		}
		label {
			display: block;
			margin-bottom: 10px;
		}
		input[type="number"] {
			padding: 5px;
			font-size: 16px;
			border-radius: 5px;
			border: none;
			box-shadow: 0 0 5px #ccc;
		}
		button[type="submit"] {
			padding: 5px 10px;
			font-size: 16px;
			background-color: #007bff;
			color: #fff;
			border: none;
			border-radius: 5px;
			cursor: pointer;
			transition: background-color 0.3s;
		}
		button[type="submit"]:hover {
			background-color: #0069d9;
		}
		.resultado {
			margin-top: 30px;
			text-align: center;
			background-color: #fff;
			padding: 20px;
			border-radius: 5px;
			box-shadow: 0 0 10px #ccc;
		}
	</style>
</head>
<body>
	<h1>Area y Perimetro de un Cuadrado</h1>
	<?php
		if(isset($_POST['lado'])) {
			$lado = $_POST['lado'];
			$area = $lado * $lado;
			$perimetro = 4 * $lado;
			echo "<div class='resultado'>";
			echo "<p>El area del cuadrado es: " . $area . "</p>";
			echo "<p>El perimetro del cuadrado es: " . $perimetro . "</p>";
			echo "</div>";
		}
	?>
	<form method="POST">
		<label for="lado">Ingrese la medida del lado:</label>
		<input type="number" name="lado" required>
		<button type="submit">Calcular</button>
	</form>
</body>
</html>
